Vagrant Environment for Web Developer
=======

Precise64 Ubuntu. Tested with Vagrant 1.6.3 + VirtualBox 4.3.14 on Mac

<h2>Features</h2>
<ul>
  <li>PHP 5.3.10<br>
    <ul>
      <li>apc</li>
      <li>mysql</li>
      <li>memcache</li>
      <li>xdebug</li>
      <li>gd</li>
      <li>curl</li>
      <li>mongo</li>
    </ul>
  </li>
  <li>MySQL 5.5.38</li>
  <li>Memcached 1.4.13</li>
  <li>MongoDB 2.4.10</li>
  <li>Git / Mercurial</li>
  <li>PhpMyAdmin 4.2.7 English</li>
</ul>

<h2>Configurations & Paths</h2>
<ul>
  <li>Synced Folders (configurable in ./Vagrantfile)<br>
    <ul>
      <li><strong>path</strong> - <em>guest</em>: <code>/var/www</code> <em>host</em>: <code>./www</code></li>
    </ul>
  </li>
  <li>Public IP (configurable in ./Vagrantfile)<br>
    <ul>
      <li><strong>ip</strong> - <em>host</em>: <code>192.168.33.33</code></li>
    </ul>
  </li>
  <li>MySQL (configurable in ./Vagrantfile)<br>
    <ul>
      <li><strong>password</strong> - <em>default</em>: <code>531323</code></li>
    </ul>
  </li>
  <li>MongoDB<br>
    <ul>
      <li><strong>dbpath</strong> - <em>guest</em>: <code>/var/www-db/mongo</code> <em>host</em>: <code>N/A</code> (see <a href="http://docs.mongodb.org/manual/administration/production-notes/#production-nfs">why</a>)</li>
      <li><strong>runtime configs</strong> - <em>host</em>: <code>./www/mongo.sh</code></li>
    </ul>
  </li>
  <li>PhpMyAdmin<br>
    <ul>
      <li><strong>path</strong> - <em>host</em>: <code>./www/pma</code></li>
      <li><strong>host</strong> - <em>host</em>: <code>http://pma/</code> (learn more in <a href="#setup">Setup section</a>)</li>
    </ul>
  </li>
  <li>Web Application (configurable in ./Vagrantfile)<br>
    <ul>
      <li><strong>path</strong> - <em>default</em>: <code>./www/html</code></li>
      <li><strong>database</strong> - <em>default</em>: <code>website</code></li>
      <li><strong>host</strong> - <em>default</em>: <code>loc.sitename.com</code> (learn more in <a href="#setup">Setup section</a>)</li>
    </ul>
  </li>
</ul>

<h2>Cloning</h2>
<ul>
  <li><code>git clone https://github.com/skullhole/vagrant.git</code></li>
  <li><code>cd vagrant</code></li>
  <li><code>git submodule update --init --recursive</code></li>
</ul>

<h2 id="setup">Setup</h2>
<ul>
  <li>Vagrant Plugins<br>
    <ul>
      <li>Vagrant Hostupdater <code>vagrant plugin install vagrant-hostsupdater</code></li>
      <li>Vagrant Omnibus <code>vagrant plugin install vagrant-omnibus</code></li>
    </ul>
  </li>
  
  <li>Hosts (<em>not necessary</em>)<br>
    Your hosts on host machine should look like the following: <br><br>
    <code>
    PUBLIC_IP pma
    </code>
    <br>
    <code>
    PUBLIC_IP APPLICATION_NAME 
    </code>
    <br><br>
    
    Default hosts setup as follows: <br><br>
    <code>
    192.168.33.33 pma
    </code>
    <br>
    <code>
    192.168.33.33 loc.sitename.com
    </code>
  </li>
</ul>
