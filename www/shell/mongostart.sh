#!/bin/bash

PATH="/var/www-db/mongo"
/bin/mkdir -p "$PATH"
/bin/chmod -R 0777 "$PATH"
/usr/local/bin/mongod --dbpath "$PATH" --fork --logpath "$PATH/mongodb.log" --rest