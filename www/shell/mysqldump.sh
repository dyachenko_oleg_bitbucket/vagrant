#!/bin/bash

# read db name
echo "Enter the database name: "
read DBNAME

# get current directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PATH="/Users/skullhole/Dropbox/Work/Dumps/$DBNAME"

#
/bin/mkdir "$PATH"


# get clean and destination paths
CLEAN="$PATH/clean.$DBNAME.sql"
DUMP=$(/bin/date +"$DBNAME.%Y.%m.%d.%H.%M.%S.sql")

# exec clean
/Applications/XAMPP/xamppfiles/bin/mysql $DBNAME -u root --password=  < "$CLEAN"
/Applications/XAMPP/xamppfiles/bin/mysqldump $DBNAME -u root --password=  > "$PATH/$DUMP"
/usr/bin/zip -j "$PATH/$DUMP.zip" "$PATH/$DUMP"
/bin/rm "$PATH/$DUMP"

exit
