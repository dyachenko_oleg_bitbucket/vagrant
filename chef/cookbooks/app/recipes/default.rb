#
# Cookbook Name:: app
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

apache_conf 'httpd-vhosts' do
  enable true
end

include_recipe "database::mysql"

# create new database
mysql_database node['app']['database'] do
  connection ({:host => 'localhost', :username => 'root', :password => node['mysql']['server_root_password']})
  action :create
end

# create folder for your application
directory node["app"]["path"] do
  owner "root"
  group "root"
  mode "0755"
  action :create
  recursive true
end

# create VirtualHost
web_app 'app' do
  template 'vhost.erb'
  docroot node['app']['path']
  server_name node['app']['server_name']
  server_email node['app']['server_email']
end

web_app "pma" do
  template 'vhost.erb'
  docroot "/var/www/pma"
  server_name "pma"
  server_email node['app']['server_email']
  allow_override "All"
end

package "git" do
    action :install
end

package "mercurial" do
    action :install
end

# start mongo
cmd = "sh /var/www/shell/mongostart.sh"
%x[ #{cmd} ]

# xdebug
php_pear "xdebug" do
  # Specify that xdebug.so must be loaded as a zend extension
  zend_extensions ['xdebug.so']
  action :install
end

# mongo
php_pear "mongo" do
  action :install
end